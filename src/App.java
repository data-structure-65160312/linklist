public class App {
    public static void main(String[] args) throws Exception {
        LinkList thelist = new LinkList();

        thelist.insertFirst(22, 2.99);
        thelist.insertFirst(44, 4.99);
        thelist.insertFirst(66, 6.99);
        thelist.insertFirst(88, 8.99);
        thelist.insertFirst(11, 1.99);
        System.out.println("---1");
        thelist.displayList();
        // thelist.deleteLast();
        // System.out.println("---2");
        thelist.delete(11);
        thelist.displayList();
        // while (!thelist.isEmpty()) {
        //     Link aLink = thelist.deleteFirst();
        //     System.out.println("Deleted ");
        //     aLink.displayLink();
        //     System.out.println("--------------------------------");
        // }
        // thelist.displayList();
    }

}
