public class LinkList {

    private Link first;

    public LinkList() // constructor
    {
        first = null; // no items on list yet
    }

    public boolean isEmpty() // true if list is empty
    {
        return (first == null);
    }

    public void insertFirst(int i, double d) {
        Link newLink = new Link(i, d);
        newLink.next = first;
        first = newLink;
    }

    public void displayList() {
        System.out.print("List (first-->last): ");
        Link current = first; // start at beginning of list
        while (current != null) // until end of list,
        {
            current.displayLink(); // print data
            current = current.next; // move to next link
        }
        System.out.println("\n");
    }

    public Link deleteFirst() {
        Link tmp = first;
        first = first.next;
        return tmp;
    }

    public Link deleteLast() {
        Link tmp = first;
        while (true) {
            if (tmp.next.next == null) {
                Link last = tmp.next;
                tmp.next = null;
                return last;
            }
            tmp = tmp.next;
        }
    }

    public Link find(int key) {
        Link current = first; // start at ‘first’
        while (current.iData != key) // while no match,
        {
            if (current.next == null) // if end of list,
                return null; // didn’t find it
            else // not end of list,
                current = current.next; // go to next link
        }
        return current; // found it
    }

    public Link delete(int key) // delete link with given key
    {
        Link current = first; // search for link
        Link previous = first;
        while (current.iData != key) {
            if (current.next == null)
                return null; // didn’t find it
            else {
                previous = current;
                current = current.next;
            }
        }
        if (current == first) // if first link,
            first = first.next; // change first
        else // otherwise,
            previous.next = current.next; // bypass it
        return current;

    }
}
